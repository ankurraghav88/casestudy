import React from 'react';
import './App.scss';
import CharacterCards from './pages/CharacterCards';

function App() {
  return (
    <div className="App">
      <div className="App-container">
        <CharacterCards />

      </div>
    </div>
  );
}

export default App;
