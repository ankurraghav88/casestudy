/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable jsx-a11y/label-has-associated-control */
import React from 'react';
import PropTypes from 'prop-types';

/**
 * Generic Form Label
 * @param {React.Node | array} children The child node or array of children nodes
 * @param {string} [className=null] The optional className that allows a custom style class applied
 * @return {React.Node} The node making up the component
 * @constructor
 */
const Label = ({
  children,
  className,
  ...props
}) => (
  <label className={className} {...props}>{children}</label>
);

/**
 * @typedef {Object} propTypes The prop types definitions for the component props
 * @private
 */
Label.propTypes = {
  children: PropTypes.string,
  className: PropTypes.string,
};

/**
 * @typedef {Object} defaultProps The default values for the component props
 * @private
 */
Label.defaultProps = {
  children: null,
  className: null,
};

export default Label;
