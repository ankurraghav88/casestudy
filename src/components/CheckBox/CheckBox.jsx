/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable jsx-a11y/label-has-associated-control */
import React from 'react';
import PropTypes from 'prop-types';
import './CheckBox.scss';

const CheckBox = ({
  text, onChange, defaultChecked, name, ...props
}) => (
  <div className="checkbox-container">
    <input
      type="radio"
      name={name}
      id=""
      onChange={(event) => onChange(event)}
      checked={defaultChecked}
      {...props}
    />
    <label>{text}</label>
  </div>
);

/**
 * @typedef {Object} propTypes The prop types definitions for the component props
 * @private
 */
CheckBox.propTypes = {
  text: PropTypes.string,
  onChange: PropTypes.func,
  defaultChecked: PropTypes.bool,
  name: PropTypes.string,
};

/**
 * @typedef {Object} defaultProps The default values for the component props
 * @private
 */
CheckBox.defaultProps = {
  text: '',
  onChange: () => {},
  defaultChecked: null,
  name: '',
};

export default CheckBox;
