import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Label from '../Label';
import './Search.scss';

const Search = ({ setFilter, selectedFilter }) => {
  const [state, setState] = useState('');
  const handleChange = (event) => {
    const { value } = event.target;
    setState(value);
  };
  return (
    <div className="search-box">
      <Label className="search-label">Search By Name</Label>
      <input className="search-input" type="text" name="" id="" onChange={(e) => handleChange(e)} />
      <button type="button" className="search-button" onClick={() => setFilter({ ...selectedFilter, name: state })}>Search</button>
    </div>
  );
};

/**
 * @typedef {Object} propTypes The prop types definitions for the component props
 * @private
 */
Search.propTypes = {
  selectedFilter: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.string,
  ]),
  setFilter: PropTypes.func,
};

/**
   * @typedef {Object} defaultProps The default values for the component props
   * @private
   */
Search.defaultProps = {
  selectedFilter: {},
  setFilter: () => {},
};

export default Search;
