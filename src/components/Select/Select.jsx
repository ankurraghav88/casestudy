import React from 'react';
import PropTypes from 'prop-types';
import './Select.scss';

const Select = ({
  setFilter, selectedFilter,
}) => {
  const handleChange = (e) => {
    const { value } = e.target;
    setFilter({ ...selectedFilter, sortBy: value });
  };
  return (
    <select className="select-box" onChange={(e) => handleChange(e)}>
      <option value="acending">Acending</option>
      <option value="decending">Decending</option>
    </select>
  );
};

/**
 * @typedef {Object} propTypes The prop types definitions for the component props
 * @private
 */
Select.propTypes = {
  selectedFilter: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.string,
  ]),
  setFilter: PropTypes.func,
};

/**
     * @typedef {Object} defaultProps The default values for the component props
     * @private
     */
Select.defaultProps = {
  selectedFilter: {},
  setFilter: () => {},
};

export default Select;
