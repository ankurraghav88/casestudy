import React from 'react';
import PropTypes from 'prop-types';
import { localDate } from '../../utils/localDate';
import './Card.scss';

export const Card = ({ data }) => (
  <div className="card-container">
    {data.length > 0
        && data.map((card) => (
          <div className="cardBox" key={card.id}>
            <div className="image-box">
              <img
                src={card.image}
                alt={card.name}
              />
              <div className="name-created">
                <div className="character-name">
                  <span>{card.name}</span>
                </div>
                <div className="character-created">
                  <span>
                    {' '}
                    id:
                    {card.id}
                  </span>
                  <span>
                    created on
                    {localDate(card.created)}
                  </span>
                </div>
              </div>
            </div>
            <ul className="cardBox-details">
              <li>
                <span>STATUS</span>
                <span>{card.status}</span>
              </li>
              <li>
                <span>SPECIES</span>
                <span>{card.species}</span>
              </li>
              <li>
                <span>GENDER</span>
                <span>{card.gender}</span>
              </li>
              <li>
                <span>ORIGIN</span>
                <span>{card.origin.name}</span>
              </li>
              <li>
                <span>LAST LOCATION</span>
                <span>{card.location.name}</span>
              </li>
            </ul>
          </div>
        ))}
  </div>
);

/**
 * @typedef {Object} propTypes The prop types definitions for the component props
 * @private
 */
Card.propTypes = {
  data: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.string,
      image: PropTypes.string,
      name: PropTypes.string,
      created: PropTypes.string,
      species: PropTypes.string,
      status: PropTypes.string,
      gender: PropTypes.string,
    })),
  ]),
};

/**
 * @typedef {Object} defaultProps The default values for the component props
 * @private
 */
Card.defaultProps = {
  data: [],
};
