import React from 'react';
import PropTypes from 'prop-types';
import { Card } from './Card';
import Search from '../Search';
import Select from '../Select';


const Cards = ({
  data, selectedFilter, setFilter,
}) => (
  <div className="cards-section">
    <div className="search-select">
      <Search data={data} selectedFilter={selectedFilter} setFilter={setFilter} />
      <Select setFilter={setFilter} selectedFilter={selectedFilter} />
    </div>
    <Card data={data} />
  </div>
);

/**
 * @typedef {Object} propTypes The prop types definitions for the component props
 * @private
 */
Cards.propTypes = {
  data: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.string,
      image: PropTypes.string,
      name: PropTypes.string,
      created: PropTypes.string,
      species: PropTypes.string,
      status: PropTypes.string,
      gender: PropTypes.string,
    })),
  ]),
  selectedFilter: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.string,
  ]),
  setFilter: PropTypes.func,
};

/**
 * @typedef {Object} defaultProps The default values for the component props
 * @private
 */
Cards.defaultProps = {
  data: [],
  selectedFilter: {},
  setFilter: () => {},
};

export default Cards;
