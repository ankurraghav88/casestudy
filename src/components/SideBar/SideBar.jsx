import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import CheckBox from '../CheckBox';
import Label from '../Label';
import './SideBar.scss';

const SideBar = ({
  callFilter, selectedFilter, setFilter,
}) => {
  useEffect(() => {
    callFilter(selectedFilter);
  }, [callFilter, selectedFilter]);

  const handleChange = (e, filter) => {
    setFilter({ ...selectedFilter, ...filter });
  };
  return (
    <div className="sidebar">
      <h1>Filters</h1>
      <div className="filter-box">
        <Label className="filter-label">Species</Label>
        <div className="sidebar-checkbox">
          <CheckBox name="species" value="Human" text="Human" onChange={(e) => handleChange(e, { species: 'Human' })} />
          <CheckBox name="species" value="Mytholog" text="Mytholog" onChange={(e) => handleChange(e, { species: 'Mytholog' })} />
          <CheckBox name="species" text="Humanoid" value="Humanoid" onChange={(e) => handleChange(e, { species: 'Humanoid' })} />
        </div>
      </div>

      <div className="filter-box">
        <Label className="filter-label">Gender</Label>
        <div className="sidebar-checkbox">
          <CheckBox name="gender" value="male" text="Male" onChange={(e) => handleChange(e, { gender: 'Male' })} />
          <CheckBox name="gender" value="female" text="Female" onChange={(e) => handleChange(e, { gender: 'Female' })} />
        </div>
      </div>

    </div>

  );
};

/**
 * @typedef {Object} propTypes The prop types definitions for the component props
 * @private
 */
SideBar.propTypes = {
  selectedFilter: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.string,
  ]),
  setFilter: PropTypes.func,
  callFilter: PropTypes.func,
};

/**
       * @typedef {Object} defaultProps The default values for the component props
       * @private
       */
SideBar.defaultProps = {
  selectedFilter: {},
  setFilter: () => {},
  callFilter: () => {},
};

export default SideBar;
