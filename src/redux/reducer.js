import { combineReducers } from 'redux';
import CharacterCardsReducer from '../pages/CharacterCards/CharacterCards.reducer';

const rootReducer = combineReducers({
  CharacterCardsReducer,
});

export default rootReducer;
