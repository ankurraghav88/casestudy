import axios from 'axios';

export const get = (url) => {
  try {
    const response = axios
      .get(url, {
        headers: {
          'Content-Type': 'application/json',
        },
      });

    return response;
  } catch (e) {
    return e;
  }
};
