import {
  put, takeLatest, all, call,
} from 'redux-saga/effects';
import { get } from '../api';
import {
  CALL_CHARACTER_SUCCESS,
  CALL_CHARACTER,
  CALL_FILTER,
  CALL_FILTER_SUCCESS,
} from '../constant/constant';

function* getCharacterData() {
  const charaterApiEndPoint = 'https://rickandmortyapi.com/api/character/';
  try {
    const { data: { results } } = yield call(get, charaterApiEndPoint);
    yield put({
      type: CALL_CHARACTER_SUCCESS,
      data: results,
    });
  } catch (e) {
    // eslint-disable-next-line no-console
    console.log(e);
  }
}

function* getFilterCharacterData({ payload }) {
  const {
    species, gender, name, sortBy,
  } = payload;
  const filterApiEndPoint = `https://rickandmortyapi.com/api/character/?species=${species}&gender=${gender}&name=${name}`;
  try {
    const { data: { results } } = yield call(get, filterApiEndPoint);
    let data;
    if (sortBy === 'acending') {
      data = [...results].sort((a, b) => a.id - b.id);
    } else if (sortBy === 'decending') {
      data = [...results].sort((a, b) => b.id - a.id);
    }

    yield put({
      type: CALL_FILTER_SUCCESS,
      data,
    });
  } catch (e) {
    // eslint-disable-next-line no-console
    console.log(e);
  }
}

function* actionWatcher() {
  yield takeLatest(CALL_CHARACTER, getCharacterData);
  yield takeLatest(CALL_FILTER, getFilterCharacterData);
}

export default function* rootSaga() {
  yield all([actionWatcher()]);
}
