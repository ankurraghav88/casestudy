export const localDate = (date) => new Date(date).toLocaleString('default', { year: 'numeric', month: 'long', day: 'numeric' });
