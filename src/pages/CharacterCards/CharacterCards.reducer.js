import _ from 'lodash';

import {
  CALL_CHARACTER_SUCCESS,
  SEARCH_FILTER,
  CALL_FILTER_SUCCESS,
  SET_FILTER,
} from '../../constant/constant';

const initialState = {
  characterData: [],
  masterData: [],
  filter: {
    species: '', gender: '', name: '', sortBy: 'acending',
  },
};

const CharacterCardsReducer = (state = initialState, action) => {
  switch (action.type) {
    case CALL_CHARACTER_SUCCESS:
      return {
        ...state,
        characterData: action.data,
        masterData: action.data,
      };
    case CALL_FILTER_SUCCESS:
      return {
        ...state,
        characterData: action.data,
      };
    case SET_FILTER:
      return {
        ...state,
        filter: action.payload,
      };
    case SEARCH_FILTER:
      return {
        ...state,
        characterData: _.filter(
          state.masterData,
          ({ name }) => name.toLowerCase().indexOf(action.payload) !== -1,
        ),
      };
    default:
      return state;
  }
};

export default CharacterCardsReducer;
