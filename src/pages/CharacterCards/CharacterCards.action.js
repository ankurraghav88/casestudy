import {
  CALL_CHARACTER,
  SEARCH_FILTER,
  CALL_FILTER,
  SET_FILTER,
} from '../../constant/constant';

export const callCharacter = () => ({
  type: CALL_CHARACTER,
});

export const callFilter = (payload) => ({
  type: CALL_FILTER,
  payload,
});

export const setFilter = (payload) => ({
  type: SET_FILTER,
  payload,
});

export const search = (payload) => ({
  type: SEARCH_FILTER,
  payload,
});
