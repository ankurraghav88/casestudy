import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Cards from '../../components/Cards';
import SideBar from '../../components/SideBar';
import {
  callCharacter as callCharacterAction,
  callFilter as callFilterAction, setFilter as setFilterAction,
} from './CharacterCards.action';

const CharacterCards = ({
  callCharacter, characterData, callFilter, selectedFilter, setFilter,
}) => {
  const [state, setState] = useState([]);

  useEffect(() => {
    callCharacter();
  }, [callCharacter]);

  useEffect(() => {
    setState(characterData);
  }, [characterData]);


  return (
    <>
      <SideBar callFilter={callFilter} selectedFilter={selectedFilter} setFilter={setFilter} />
      <Cards
        data={state}
        selectedFilter={selectedFilter}
        setFilter={setFilter}
      />
    </>
  );
};

const mapStateToProps = (state) => {
  const { characterData, filter } = state.CharacterCardsReducer;
  return {
    characterData,
    selectedFilter: filter,
  };
};

const mapDispatchToProps = (dispatch) => ({
  callCharacter: () => {
    dispatch(callCharacterAction());
  },
  callFilter: (data) => {
    dispatch(callFilterAction(data));
  },
  setFilter: (data) => {
    dispatch(setFilterAction(data));
  },
});

/**
 * @typedef {Object} propTypes The prop types definitions for the component props
 * @private
 */
CharacterCards.propTypes = {
  selectedFilter: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.string,
  ]),
  setFilter: PropTypes.func,
  callCharacter: PropTypes.func,
  callFilter: PropTypes.func,
  characterData: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.string,
      image: PropTypes.string,
      name: PropTypes.string,
      created: PropTypes.string,
      species: PropTypes.string,
      status: PropTypes.string,
      gender: PropTypes.string,
    })),
  ]),
};

/**
* @typedef {Object} defaultProps The default values for the component props
* @private
*/
CharacterCards.defaultProps = {
  selectedFilter: {},
  callCharacter: () => {},
  callFilter: () => {},
  setFilter: () => {},
  characterData: [],
};

export default connect(mapStateToProps, mapDispatchToProps)(CharacterCards);
